"""
Show test with Gitlab CI
"""


def hello_world():
    """
    Hello world return message that does display console
    """
    return "Hello World CI/CD"


if __name__ == "__main__":
    print(hello_world())
